//	developing the mesh for a 2D pipe flow for the use of blockMesh

#include <cstdio>
#include "math.h"
#include "meshIO.h"
#include <string>
#include "grid.h"

int main(){
	printf("\nHello, I am meshGen for pipe flow.\n\n");
	double wx, A0, psi, phi, gamma;
	double* pdCrd;
	pdCrd = new double[6];	// coordinate array: pdCrd[0] = x0, pdCrd[1] = x1, pdCrd[2] = y0,..... 
	
	string *pStEdgeLine;
	pStEdgeLine = new string[5];

	const char* pCChGrdFlNme = "blockMeshDict";

	// ------------------- setting grid according to parameter file:-------------------------------
	grid gr( pCChGrdFlNme );

	wx = gr.lenY;
	A0 = gr.volFac * wx*wx * gr.PI * 0.25;

	psi = (double(gr.pipeFactor) + 1.0) * 2.0*gr.PI;
	pdCrd = gr.updateCoordinates(psi);

	gr.appendLine(pCChGrdFlNme, "\n\nvertices\n(\n");

	gr.writeVertices(0,pdCrd);
		
	phi = psi;
	for( int i=1; i<=gr.noBl; i++){
		psi = phi;

		// new angle:
		phi = gr.PI + sqrt( gr.PI*gr.PI + psi*psi - 2.0*gr.PI*psi + (4.0*gr.PI)*A0/(wx*wx) );
		if (gr.debugOutput) printf("\tnext angle: phi_%d = %f pi\n",i,phi/gr.PI);
		
		// new coordinates for refinement of grid:
		// edges written to diferent file (ext)
		gr.extWriteEdges(pStEdgeLine, i, 1);

		for( int j=1; j<gr.resX; j++){
			gamma = (phi-psi)*double(j)/double(gr.resX) + psi;
			pdCrd = gr.updateCoordinates(gamma);	// coordinates for spline supports
			gr.extWriteEdgeSupport(pStEdgeLine, pdCrd);
		}
		gr.extWriteEdges(pStEdgeLine, i, 0);

		// writing new vertex coordinates:
		pdCrd = gr.updateCoordinates(phi);
		gr.writeVertices(i,pdCrd);
	}
	gr.appendLine(pCChGrdFlNme, ");\n");

	// ------------------- defining blocks: ------------------------------------
	gr.writeBlocks(gr.noBl,gr.resX,gr.resY,gr.resZ);

	// ------------------- defining edges: ------------------------------------
	gr.writeEdges();

	// // ------------------- defining boundaries: ------------------------------------
	gr.writeBoundaries(gr.noBl);

	// // ------------------- merging patches: ------------------------------------
	gr.appendLine(pCChGrdFlNme, "\n\nmergePatchPairs\n(\n);\n");

	return 0;
}






