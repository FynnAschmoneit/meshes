//grid.cpp

#include "grid.h"

using namespace std;


grid::grid( const char* pCChNme ){
	pCChOutPutFileName = pCChNme;
	pCChExtFileName = "edgeFile.txt";

	pchStreamIn = new char[STREAMSIZE];
	ifstream iStrHeaderFile("header.txt");
	ofstream oStrMeshFile(pCChOutPutFileName, ofstream::trunc );		// open empty file and append to it!
	ofstream oStrEdgeFile( pCChExtFileName, ofstream::trunc );		// open empty file and append to it!
    while( iStrHeaderFile.getline(pchStreamIn,STREAMSIZE) != 0){
    	oStrMeshFile << pchStreamIn << endl;
    };


	printf("grid::grid\t\tinitialsing grid from parameter file.\n");
	int a = readParameters();
	if(a!=0){
		printf("wrong entry, check parameters\n");
		getchar();
	}
	PI = 3.1415926535;
	debugOutput = false;

	// calculating number of blocks of size A0 by the length of the sheet:
	double Amin = volFac*lenY*lenY*PI*0.25;
	noBl = int(lenX*lenY/Amin);
	printf("using %d blocks. unused length = %f\n",noBl,lenX*lenY/Amin-double(noBl));
}



void grid::writeBoundaries(int noBlo){
	printf("meshIO::writeBoundaries\t\twriting boundaries\n");
	ofstream oStrMeshFile(pCChOutPutFileName, ofstream::app );
	int v0,v1,v2,v3,v4,v5,v6,v7;
	
	oStrMeshFile << "\n\nboundary\n(\n";
	oStrMeshFile << "\tinlet\n\t{\n\t\ttype patch;\n\t\tfaces\n\t\t(\n\t\t\t(0 1 3 2)\n\t\t);\n\t}\n";

	v0 = 0; v1 = 2; 
	v2 = 1; v3 = 5;
	v4 = 2; v5 = 3;
	v6 = 0; v7 = 4;
	oStrMeshFile << "\tfixedWalls\n\t{\n\t\ttype wall;\n\t\tfaces\n\t\t(\n";

	for( int i=1; i<=noBlo; i++){
		// inside:
		oStrMeshFile << "\t\t\t(" << v0 << " " << v1 << " " << v1+4 << " " << v0+4 << ")\n";
		v0 += 4;
		v1 += 4;
		// outside:
		oStrMeshFile << "\t\t\t(" << v2 << " " << v3 << " " << v3+2 << " " << v2+2 << ")\n";
		v2 += 4;
		v3 += 4;
		// front:
		oStrMeshFile << "\t\t\t(" << v4 << " " << v5 << " " << v5+4 << " " << v4+4 << ")\n";
		v4 += 4;
		v5 += 4;
		// back:
		oStrMeshFile << "\t\t\t(" << v6 << " " << v7 << " " << v7+1 << " " << v6+1 << ")\n";
		v6 += 4;
		v7 += 4;
	}

	oStrMeshFile << "\t\t);\n\t}\n\toutlet\n\t{\n\t\ttype patch;\n\t\tfaces\n\t\t(\n";

	v0 = 4*noBlo;
	v1 = v0 + 2;
	v2 = v0 + 3;
	v3 = v0 + 1;

	oStrMeshFile << "\t\t\t(" << v0 << " " << v1 << " " << v2 << " " << v3 << ")\n";
	oStrMeshFile << "\t\t);\n\t}\n);\n" ;
}

void grid::writeBlocks(int noBlo, int resX, int resY, int resZ){
	ofstream oStrMeshFile(pCChOutPutFileName, ofstream::app );
	printf("meshIO::writeBlocks\t\twriting blocks\n");
	int v0,v1,v2,v3,v4,v5,v6,v7;
	v0 = 0; v1 = 1; v2 = 2; v3 = 3; v4 = 4; v5 = 5; v6 = 6; v7 = 7;

	oStrMeshFile << "\n\nblocks\n(\n";

	oStrMeshFile << "\thex ("<< v0 << " " << v1 << " " << v5 << " " << v4 << " " << v2 << " " << v3 << " " << v7 << " " << v6 << ") (" 
		<< resY << " " << resX << " " << resZ << ")  simpleGrading (1 1 1)\n" ;
	for(int i=1; i<noBlo; i++){

		v0 = v4;
		v1 = v5;
		v2 = v6;
		v3 = v7;

		v4 = v0 + 4;
		v5 = v1 + 4;
		v6 = v2 + 4;
		v7 = v3 + 4;

		oStrMeshFile << "\thex ("<< v0 << " " << v1 << " " << v5 << " " << v4 << " " << v2 << " " << v3 << " " << v7 << " " << v6 << ") (" 
		<< resY << " " << resX << " " << resZ << ")  simpleGrading (1 1 1)\n" ;

	}
	oStrMeshFile << ");\n";
}

void grid::writeVertices(int no, double* ar){
	ofstream oStrMeshFile(pCChOutPutFileName, ofstream::app );

	oStrMeshFile.precision(15);
	oStrMeshFile << fixed;
	oStrMeshFile << "\t(" << ar[0] << " " << ar[2] << " " << ar[4] << ")\t\t// " << no*4 << endl ;
	oStrMeshFile << "\t(" << ar[1] << " " << ar[3] << " " << ar[4] << ")\t\t// " << no*4+1 << endl ;
	oStrMeshFile << "\t(" << ar[0] << " " << ar[2] << " " << ar[5] << ")\t\t// " << no*4+2 << endl ;
	oStrMeshFile << "\t(" << ar[1] << " " << ar[3] << " " << ar[5] << ")\t\t// " << no*4+3 << endl ;
}


void grid::appendLine(const char* pCChFileName, string pStLineContent){
	
	ofstream oStrMeshFile(pCChFileName, ofstream::app );
	
	oStrMeshFile << pStLineContent ;
}


int grid::readParameters(){
	
	int streamsize = 200;
 	char* streamIn = new char[streamsize];
 	// char* handOver = new char[streamsize];

	fstream meshParameters;
	meshParameters.open("meshParameters.txt");

	meshParameters.getline(streamIn,streamsize);
	meshParameters.getline(streamIn,streamsize);
	meshParameters.getline(streamIn,streamsize);
	meshParameters.getline(streamIn,streamsize);
// origins
	orX=atof(streamIn);
	meshParameters.getline(streamIn,streamsize);
	meshParameters.getline(streamIn,streamsize);
	orY=atof(streamIn);
	meshParameters.getline(streamIn,streamsize);
	meshParameters.getline(streamIn,streamsize);
	orZ=atof(streamIn);
	meshParameters.getline(streamIn,streamsize);
	meshParameters.getline(streamIn,streamsize);
// length,hight, thickness
	if ( atof(streamIn) != 0.0) lenX=atof(streamIn); else return 1;
	meshParameters.getline(streamIn,streamsize);
	meshParameters.getline(streamIn,streamsize);
	if ( atof(streamIn) != 0.0) lenZ=atof(streamIn); else return 1;
	meshParameters.getline(streamIn,streamsize);
	meshParameters.getline(streamIn,streamsize);
	if ( atof(streamIn) != 0.0) lenY=atof(streamIn); else return 1;
	meshParameters.getline(streamIn,streamsize);
	meshParameters.getline(streamIn,streamsize);
//resolutions
	if ( atoi(streamIn) != 0.0) resX=atoi(streamIn); else return 1;
	meshParameters.getline(streamIn,streamsize);
	meshParameters.getline(streamIn,streamsize);
	if ( atoi(streamIn) != 0.0) resZ=atoi(streamIn); else return 1;
	meshParameters.getline(streamIn,streamsize);
	meshParameters.getline(streamIn,streamsize);
	if ( atoi(streamIn) != 0.0) resY=atoi(streamIn); else return 1;
	meshParameters.getline(streamIn,streamsize);
	meshParameters.getline(streamIn,streamsize);
// inner pipe factor, number of blocks, initial vomule factor
	if ( atoi(streamIn) != 0.0) pipeFactor=atoi(streamIn); else return 1;
	meshParameters.getline(streamIn,streamsize);
	meshParameters.getline(streamIn,streamsize);
	if ( atof(streamIn) != 0.0) sheetGap=atof(streamIn); else return 1;
	meshParameters.getline(streamIn,streamsize);
	meshParameters.getline(streamIn,streamsize);
	if ( atoi(streamIn) != 0.0) noBl=atoi(streamIn); else return 1;
	meshParameters.getline(streamIn,streamsize);
	meshParameters.getline(streamIn,streamsize);
	if ( atof(streamIn) != 0.0) volFac=atof(streamIn); else return 1;

	printf("grid parameters:\n\
		\torX = %.1f, orY = %.1f, orZ = %.1f \n\
		\tlength = %.4f, width = %.4f, thickness = %.4f \n\
		\tresolution length = %d, res width = %d, res thickness = %d\n\
		\tpipe factor = %d, sheet gap = %.2f number of blocks = %d, initial volume factor = %.2f\n"
		,orX, orY, orZ,lenX,lenZ, lenY, resX, resZ, resY, pipeFactor,sheetGap, noBl, volFac);

    meshParameters.close();
	return 0;
}

void grid::extWriteEdgeSupport(string *pStEL, double *pdCrd){
// add corresponding support points here:

	pStEL[0] += " ( " + to_string(pdCrd[0]) + " " + to_string(pdCrd[2]) + " " + to_string(pdCrd[4]) + " )";
	pStEL[1] += " ( " + to_string(pdCrd[0]) + " " + to_string(pdCrd[2]) + " " + to_string(pdCrd[5]) + " )";
	pStEL[2] += " ( " + to_string(pdCrd[1]) + " " + to_string(pdCrd[3]) + " " + to_string(pdCrd[4]) + " )";
	pStEL[3] += " ( " + to_string(pdCrd[1]) + " " + to_string(pdCrd[3]) + " " + to_string(pdCrd[5]) + " )";
}

void grid::extWriteEdges(string *pStEL, int iBlNo, bool bMode ){

	if(bMode){
		// connecting edges:

		int nr = iBlNo*4;
		pStEL[0] = "\tspline " + to_string(nr-4) + " " + to_string(nr) + " ( ";
		pStEL[1] = "\tspline " + to_string(nr-2) + " " + to_string(nr+2) + " ( ";
		pStEL[2] = "\tspline " + to_string(nr-3) + " " + to_string(nr+1) + " ( ";
		pStEL[3] = "\tspline " + to_string(nr-1) + " " + to_string(nr+3) + " ( ";
	} else {
		// closing brackets and export data to file:

		pStEL[0] += " )\n";
		pStEL[1] += " )\n";
		pStEL[2] += " )\n";
		pStEL[3] += " )\n";

		//exporting data:

		ofstream oStrEdgeFile( pCChExtFileName, ofstream::app );
		oStrEdgeFile << pStEL[0];
		oStrEdgeFile << pStEL[1];
		oStrEdgeFile << pStEL[2];
		oStrEdgeFile << pStEL[3];
	}
}

void grid::writeEdges(){

	char *pchStreamIn = new char[STREAMSIZE2];
	ifstream iStrEdgeFile( pCChExtFileName);
	ofstream oStrMeshFile(pCChOutPutFileName, ofstream::app );
	
	oStrMeshFile << "\n\nedges\n(" << endl;

	while( iStrEdgeFile.getline(pchStreamIn, STREAMSIZE2) != 0){
		oStrMeshFile << pchStreamIn << endl;
	}

	oStrMeshFile << ");" << endl;
}

double* grid::updateCoordinates(double angle){
	double* ar;
	ar = new double[6];
	ar[0] = Xin(angle);
	ar[1] = Xout(angle);
	ar[2] = Yin(angle);
	ar[3] = Yout(angle);
	ar[4] = orZ;
	ar[5] = orZ+lenZ;
	return ar;
}

double grid::Xin(double beta){
	return lenY/(2.0*PI) * ( beta - 2.0*PI )  * cos(beta);
}

double grid::Yin(double beta){
	return lenY/(2.0*PI) * ( beta - 2.0*PI )  * sin(beta);
}

double grid::Xout(double beta){
	return ( lenY/(2.0*PI) * beta - lenY*sheetGap ) * cos(beta);
}

double grid::Yout(double beta){
	return ( lenY/(2.0*PI) * beta - lenY*sheetGap) * sin(beta);
}
