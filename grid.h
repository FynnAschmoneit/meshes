// grid.h
#include <cstdio>
#include <cstdlib>		// for atof
#include "math.h"
#include <fstream>
#include <string>
# define STREAMSIZE 1000
# define STREAMSIZE2 10000

using namespace std;


class grid{

	private:
		char **ppDelim;
		char *pchStreamIn, *pchEdgeFile ;
		const char* pCChOutPutFileName;
		const char* pCChExtFileName;

	public:

		bool debugOutput;
		double orX, orY, orZ, lenX, lenY, lenZ, PI, volFac, sheetGap;
		int resX, resY, resZ, noBl, pipeFactor;

		grid( const char* pCChNme );
		int readParameters();
		void writeVertices(int no, double* ar);
		void writeBlocks(int noBlo, int resX, int resY, int resZ);
		void writeBoundaries(int noBlo);
		double* updateCoordinates(double angle);
		void extWriteEdges(string *pStEL, int iBlNo, bool bMode);
		void extWriteEdgeSupport(string *pStEL, double *pdCrd);
		void writeEdges();
		void appendLine(const char* pCChFileName, string pStLineContent);

		double Xin(double beta);
		double Yin(double beta);
		double Xout(double beta);
		double Yout(double beta);
};

