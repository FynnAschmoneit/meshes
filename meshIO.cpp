// // class for i/o operation, creating blockMeshDict

// #include "meshIO.h"

// meshIO::meshIO(){
// 	pchStreamIn = new char[STREAMSIZE];
	
// 	// copying headerFile:
// 	ifstream iStrHeaderFile("header.txt");
// 	ofstream oStrMeshFile("blockMeshDict", ofstream::trunc );		// open empty file and append to it!
// 	ofstream oStrEdgeFile("edgeFile.txt", ofstream::trunc );		// open empty file and append to it!
//     while( iStrHeaderFile.getline(pchStreamIn,STREAMSIZE) != 0){
//     	oStrMeshFile << pchStreamIn << endl;
//     };
// 	printf("meshIO::meshIO\t\theader file copied.\n");
// }

// // writing respective vertices to file; If x0 = -1, it writes header. If x1 = -1, it writes bottom line.
// // void meshIO::writeVertices(int no, double* ar){
// // 	ofstream oStrMeshFile("blockMeshDict", ofstream::app );

// // 	oStrMeshFile.precision(15);
// // 	oStrMeshFile << fixed;
// // 	oStrMeshFile << "\t(" << ar[0] << " " << ar[2] << " " << ar[4] << ")\t\t// " << no*4 << endl ;
// // 	oStrMeshFile << "\t(" << ar[1] << " " << ar[3] << " " << ar[4] << ")\t\t// " << no*4+1 << endl ;
// // 	oStrMeshFile << "\t(" << ar[0] << " " << ar[2] << " " << ar[5] << ")\t\t// " << no*4+2 << endl ;
// // 	oStrMeshFile << "\t(" << ar[1] << " " << ar[3] << " " << ar[5] << ")\t\t// " << no*4+3 << endl ;
// // }

// // void meshIO::writeBlocks(int noBlo, int resX, int resY, int resZ){
// // 	ofstream oStrMeshFile("blockMeshDict", ofstream::app );
// // 	printf("meshIO::writeBlocks\t\twriting blocks\n");
// // 	int v0,v1,v2,v3,v4,v5,v6,v7;
// // 	v0 = 0; v1 = 1; v2 = 2; v3 = 3; v4 = 4; v5 = 5; v6 = 6; v7 = 7;

// // 	oStrMeshFile << "\n\nblocks\n(\n";

// // 	oStrMeshFile << "\thex ("<< v0 << " " << v1 << " " << v5 << " " << v4 << " " << v2 << " " << v3 << " " << v7 << " " << v6 << ") (" 
// // 		<< resY << " " << resX << " " << resZ << ")  simpleGrading (1 1 1)\n" ;
// // 	for(int i=1; i<noBlo; i++){

// // 		v0 = v4;
// // 		v1 = v5;
// // 		v2 = v6;
// // 		v3 = v7;

// // 		v4 = v0 + 4;
// // 		v5 = v1 + 4;
// // 		v6 = v2 + 4;
// // 		v7 = v3 + 4;

// // 		oStrMeshFile << "\thex ("<< v0 << " " << v1 << " " << v5 << " " << v4 << " " << v2 << " " << v3 << " " << v7 << " " << v6 << ") (" 
// // 		<< resY << " " << resX << " " << resZ << ")  simpleGrading (1 1 1)\n" ;

// // 	}
// // 	oStrMeshFile << ");\n";
// // }

// void meshIO::writeBoundaries(int noBlo){
// 	printf("meshIO::writeBoundaries\t\twriting boundaries\n");
// 	ofstream oStrMeshFile("blockMeshDict", ofstream::app );
// 	int v0,v1,v2,v3,v4,v5,v6,v7;
	
// 	oStrMeshFile << "\n\nboundary\n(\n";
// 	oStrMeshFile << "\tinlet\n\t{\n\t\ttype patch;\n\t\tfaces\n\t\t(\n\t\t\t(0 1 3 2)\n\t\t);\n\t}\n";

// 	v0 = 0; v1 = 2; 
// 	v2 = 1; v3 = 5;
// 	v4 = 2; v5 = 3;
// 	v6 = 0; v7 = 4;
// 	oStrMeshFile << "\tfixedWalls\n\t{\n\t\ttype wall;\n\t\tfaces\n\t\t(\n";

// 	for( int i=1; i<=noBlo; i++){
// 		// inside:
// 		oStrMeshFile << "\t\t\t(" << v0 << " " << v1 << " " << v1+4 << " " << v0+4 << ")\n";
// 		v0 += 4;
// 		v1 += 4;
// 		// outside:
// 		oStrMeshFile << "\t\t\t(" << v2 << " " << v3 << " " << v3+2 << " " << v2+2 << ")\n";
// 		v2 += 4;
// 		v3 += 4;
// 		// front:
// 		oStrMeshFile << "\t\t\t(" << v4 << " " << v5 << " " << v5+4 << " " << v4+4 << ")\n";
// 		v4 += 4;
// 		v5 += 4;
// 		// back:
// 		oStrMeshFile << "\t\t\t(" << v6 << " " << v7 << " " << v7+1 << " " << v6+1 << ")\n";
// 		v6 += 4;
// 		v7 += 4;
// 	}

// 	oStrMeshFile << "\t\t);\n\t}\n\toutlet\n\t{\n\t\ttype patch;\n\t\tfaces\n\t\t(\n";

// 	v0 = 4*noBlo;
// 	v1 = v0 + 2;
// 	v2 = v0 + 3;
// 	v3 = v0 + 1;

// 	oStrMeshFile << "\t\t\t(" << v0 << " " << v1 << " " << v2 << " " << v3 << ")\n";
// 	oStrMeshFile << "\t\t);\n\t}\n);\n" ;
// }
